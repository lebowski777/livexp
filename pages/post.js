import React from 'react';
import { connect } from 'react-redux';
import Layout from '../src/components/Layouts/Main';
import Title from '../src/components/Title';
import ContentWrapper from "../src/components/Layouts/Content";
import { fetchPosts } from '../src/store/actions/posts';
import { getItemByValue, getLastSegmentFromUrl } from '../src/helpers';

class Post extends React.Component {
  static async getInitialProps({ store, req, isServer }) {
    if(isServer) {
      const query = getLastSegmentFromUrl(req.originalUrl);
      await store.dispatch(fetchPosts(query));
    }
  }

  render() {
    const { post } = this.props;
    return <Layout>
      <Title>{post.title}</Title>
      <ContentWrapper>
        <p>{post.body}</p>
      </ContentWrapper>
    </Layout>
  }
}

Post.defaultProps = {
  post: {}
};

const mapStateToProps = (state) => {
  return {
    post: getItemByValue(state.posts.list, getLastSegmentFromUrl(state.router.location.pathname), 'slug')[0],
  };
};

export default connect(mapStateToProps)(Post)
