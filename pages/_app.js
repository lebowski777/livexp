import React from 'react';
import {Provider} from 'react-redux';
import App, {Container} from 'next/app';
import withRedux from 'next-redux-wrapper';
import { makeStore } from '../src/store/store';
import { ConnectedRouter } from 'connected-next-router';
import { fetchPages } from '../src/store/actions/pages';

class MyApp extends App {
  static async getInitialProps({Component, ctx}) {
    const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};
    const pages = await ctx.store.dispatch(fetchPages());
    return { pageProps, pages };
  }

  render() {
    const {Component, pageProps, store, pages} = this.props;
    return (
      <Container>
        <Provider store={store}>
          <ConnectedRouter>
            <Component {...pageProps} pages={pages} />
          </ConnectedRouter>
        </Provider>
      </Container>
    );
  }
}

export default withRedux(makeStore)(MyApp);
