import React from 'react'
import Layout from '../src/components/Layouts/Main';
import Title from '../src/components/Title';
import ContentWrapper from "../src/components/Layouts/Content";

const IndexPage = () =>
  <Layout>
    <Title>What is LivExp?</Title>
    <ContentWrapper>
      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?  Aliquam ornare wisi eu metus. Duis risus. Nulla quis diam. Etiam quis quam. Aenean id metus id velit ullamcorper pulvinar. Fusce wisi.</p>

      <h2>How to install?</h2>
      <p>Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui. Maecenas libero. Nulla accumsan, elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla vel leo.</p>
      <code><pre>npm install --save livexp</pre></code>
      <p>or</p>
      <code><pre>yarn add livexp</pre></code>

      <h2>Licences</h2>
      <p>Donec vitae arcu. In sem justo, commodo ut, suscipit at, pharetra vitae, orci. Nullam rhoncus aliquam metus. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Proin pede metus, vulputate nec, fermentum fringilla, vehicula vitae, justo. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Integer in sapien. Nullam sapien sem.</p>
    </ContentWrapper>
  </Layout>

export default IndexPage
