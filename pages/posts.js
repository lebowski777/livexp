import React from 'react'
import Layout from '../src/components/Layouts/Main';
import Title from '../src/components/Title';
import Post from '../src/components/Post';
import ContentWrapper from "../src/components/Layouts/Content";
import { connect } from 'react-redux';
import { fetchPosts } from '../src/store/actions/posts';

class Posts extends React.Component {
  static async getInitialProps ({ store }) {
    await store.dispatch(fetchPosts());
  }

  render() {
    const { posts } = this.props;
    return <Layout>
      <Title>Latest posts</Title>
      <ContentWrapper>
        {posts.map(post => <Post key={post.slug} post={post}/>)}
      </ContentWrapper>
    </Layout>
  }
}

Posts.defaultProps = {
  posts: []
};

const mapStateToProps = (state) => {
  return {
    posts: state.posts.list
  };
};

export default connect(mapStateToProps)(Posts)
