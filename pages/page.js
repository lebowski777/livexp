import React from 'react';
import Layout from '../src/components/Layouts/Main';
import Title from '../src/components/Title';
import ContentWrapper from "../src/components/Layouts/Content";
import { connect } from 'react-redux';
import { getItemByValue, getLastSegmentFromUrl } from '../src/helpers';
import PublishedContent from '../src/components/PublishedContent';
import EditContent from '../src/components/AdminMode/EditContent';

class Page extends React.PureComponent {
  render() {
    const { page, adminModeIsOn } = this.props;
    return <Layout>
      <Title>{page.title}</Title>
      <ContentWrapper>
        {!adminModeIsOn && <PublishedContent page={page} />}
        {adminModeIsOn && <EditContent page={page} />}
      </ContentWrapper>
    </Layout>
  }
}

Page.defaultProps = {
  page: {},
  adminModeIsOn: false
};

const mapStateToProps = (state) => {
  return {
    page: getItemByValue(state.pages.list, getLastSegmentFromUrl(state.router.location.pathname), 'slug')[0],
    adminModeIsOn: state.adminMode.isOn,
  };
};

export default connect(mapStateToProps)(Page)
