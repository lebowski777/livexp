import * as firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

export default () => {
  try {
    const config = {
      apiKey: "AIzaSyDiuf1B00aejQFQnKNQNCiB7v1s_o6CfcM",
      authDomain: "livexp-f4c1e.firebaseapp.com",
      databaseURL: "https://livexp-f4c1e.firebaseio.com",
      projectId: "livexp-f4c1e",
      storageBucket: "livexp-f4c1e.appspot.com",
      messagingSenderId: "881424108375"
    };
    firebase.initializeApp(config);
  } catch (err) {
    if (!/already exists/.test(err.message)) {
      console.error('Firebase initialization error', err.stack);
    }
  }

  return firebase;
};
