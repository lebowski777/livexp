const getLastSegmentFromUrl = (url) => {
  return url.substr(url.lastIndexOf('/') + 1);
};

const getItemByValue = (object, value, searchBy) => {
  return object.filter(obj => {
    return obj[searchBy] === value
  });
};

const comparePagesByPosition = (a,b) => {
  if (a.position < b.position)
    return -1;
  if (a.position > b.position)
    return 1;
  return 0;
};

export {
  getLastSegmentFromUrl,
  getItemByValue,
  comparePagesByPosition
};
