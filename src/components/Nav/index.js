import { Link } from '../../routes';
import styled from 'styled-components';
import React from 'react';
import { connect } from 'react-redux';

const Wrapper = styled.nav`
  display: flex;
  padding: 1rem 2rem 0 1rem;
  align-items:center;
  
  a {
    margin: 0 1rem;
    color: #fff;
    text-decoration: none;
    font-size: 20px;
    position: relative;
    
    &:last-child {
      border-right: 0;
    }
    
    &:after {
      content: '';
      position: absolute;
      left: 0;
      display: inline-block;
      height: 1em;
      width: 100%;
      border-bottom: 1px solid;
      margin-top: 10px;
      opacity: 0;
      -webkit-transition: opacity 0.2s, -webkit-transform 0.2s;
      transition: opacity 0.2s, transform 0.2s;
      -webkit-transform: scale(0,1);
      transform: scale(0,1);
    }
    
    &:hover:after {
      opacity: 1;
      -webkit-transform: scale(1);
      transform: scale(1);
    }
  }
`;

class Nav extends React.Component {
  render() {
    const { pages } = this.props;

    return <Wrapper>
        {pages.map(page => <Link key={page.slug} route={`${page.slug}`}>
          <a>{page.title}</a>
        </Link>)}
      </Wrapper>
  }
}

Nav.defaultProps = {
  pages: []
};

const mapStateToProps = (state) => {
  return {
    pages: state.pages.list
  };
};

export default connect(mapStateToProps)(Nav)
