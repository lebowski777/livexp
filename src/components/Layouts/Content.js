import styled from 'styled-components';

const ContentWrapper = styled.div`
  padding: 1.5rem 2rem 0;
  
  *:first-child {
    margin-top: 0;
  }
`;

export default ContentWrapper;
