import Head from 'next/head'
import Header from '../Header/index'
import Footer from '../Footer/index'
import React from 'react'
import { createGlobalStyle } from 'styled-components'
import Loader from '../Loader';
import { connect } from 'react-redux';
import AdminMode from '../AdminMode';

const GlobalStyle = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Roboto:300,400');
  * {
    box-sizing: border-box;
  }
  body {
    padding: 0;
    margin: 0;
    font-family: 'Roboto', sans-serif;
    font-weight: 300;
  }
  .wrapper {
    display: flex;
    min-height: 100vh;
    flex-direction: column;
    main {
      flex: 1;
    }
    
    &.admin {
      padding-bottom: 132px;
    }
  }
  ul, li {
    margin: 0;
    padding: 0;
  }
  h1, h2, h3, h4 {
    font-weight: 300;
  }
  .main {
    position:relative;
    z-index: 1;
  }
  p {
    line-height: 1.7;
  }
  pre {
    background: #f4f4f4;
    border: 1px solid #ddd;
    border-left: 3px solid #ddd;
    color: #666;
    page-break-inside: avoid;
    font-family: monospace;
    font-size: 15px;
    line-height: 1.6;
    margin-bottom: 1.6em;
    max-width: 100%;
    overflow: auto;
    padding: 1em 1.5em;
    display: block;
    word-wrap: break-word;
  }
  button {
    font-family: 'Roboto', sans-serif;
    display: flex;
    align-items: center;
    padding: 10px 20px;
    line-height: 20px;
    font-size: 14px;
    border-radius: 20px;
    background-color: #fff;
    border: 0;
    cursor: pointer;
    outline: none;
  }
`;


class ContentWrapper extends React.Component {
  render () {
    const { loader, children, adminModeIsOn, title = 'LivExp - interactive page builder tool!' } = this.props;
    return (
      <div className={`wrapper ${adminModeIsOn ? 'admin' : ''}`}>
        <Head>
          <title>{ title }</title>
          <meta name='viewport' content='initial-scale=1.0, width=device-width' />
          <link rel='icon' type='image/png' href='/static/favicon.png' />
          <GlobalStyle />
        </Head>

        <Header title={'LivExp'} />

        {loader && <Loader />}
        <main className={'main'}>
          {!loader && children }
        </main>

        <Footer>
          LivExp - interactive page builder tool
        </Footer>
        {adminModeIsOn && <AdminMode/>}
      </div>
    )
  }
}

ContentWrapper.defaultProps = {
  loader: false,
  adminModeIsOn: false
};

const mapStateToProps = (state) => {
  return {
    loader: state.loader.loader,
    adminModeIsOn: state.adminMode.isOn
  };
};

export default connect(mapStateToProps)(ContentWrapper)
