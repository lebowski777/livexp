import React from 'react';

class PublishedContent extends React.PureComponent {
  render() {
    const { page } = this.props;
    return <div>{page.content}</div>
  }
}

PublishedContent.defaultProps = {
  page: {}
};

export default PublishedContent;
