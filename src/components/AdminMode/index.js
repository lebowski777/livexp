import React from 'react';
import styled from 'styled-components';
import Components from './Components';

const AdminModeWrapper = styled.section`
  color: #fff;
  position: fixed;
  bottom: 0;
  width: 100%;
  background: #222;
  padding: 1rem 1rem;
  z-index: 1;
`;

class AdminMode extends React.Component {
  render () {
    return (
      <AdminModeWrapper>
        <Components />
      </AdminModeWrapper>
    )
  }
}

export default AdminMode
