import React from 'react';
import styled from 'styled-components';

const ComponentsWrapper = styled.section`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  
  .component {
    width: 23%;
    margin: 0 1%;
    height: 100px;
    display: flex;
    justify-content: center;
    align-items: center;
    border: 1px solid #777;
    border-radius: 3px;
    background-color: rgba(255, 255, 255, .05);
    transition: .3s;
    cursor: pointer;
    user-select: none;
    
    &:hover {
      color: #bb8bff;
      border-color: #bb8bff;
      background-color: rgba(255, 255, 255, .1);
    }
  }
`;

class Components extends React.Component {
  render () {
    return (
      <ComponentsWrapper>
        <div className="component">Text</div>
        <div className="component">Image</div>
        <div className="component">Slider</div>
        <div className="component">Post</div>
      </ComponentsWrapper>
    )
  }
}

export default Components
