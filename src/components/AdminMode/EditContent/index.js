import React from 'react';
import styled from 'styled-components';

class EditContent extends React.PureComponent {
  render() {
    const { page } = this.props;
    const contentParsed = JSON.parse(page.content);
    return <PageContent>
      {contentParsed.rows.map((row, index) => <div className="page-row" key={`row-${index}`}>
        {row.cells.map((component, index) => <div className="page-component" key={`component-${index}`}>
          {component}
        </div>)}
      </div>)}
    </PageContent>
  }
}

const PageContent = styled.section`
  display: flex;
  justify-content: space-between;
  
  .page-component {
    
  }
  `;

export default EditContent;
