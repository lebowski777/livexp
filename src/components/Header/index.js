import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Nav from '../Nav';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCog } from '@fortawesome/free-solid-svg-icons';
import { getItemByValue, getLastSegmentFromUrl } from '../../helpers';
import { connect } from 'react-redux';
import { setAdminMode } from '../../store/actions/adminMode';
import { SET_ADMIN_MODE } from '../../store/actions';

const HeaderWrapper = styled.header`
  color: #fff;
  padding-bottom: 2.5rem;
  position: relative;
  background: #863AF2;
  background: linear-gradient(to right, #863AF2, #CD41E0);
  
  .headerWrapper {
    display: flex;
    justify-content: flex-start;
    align-items: center;
    padding: 1.5rem 2rem 0;
    
    .title {
      display: flex;
      align-items: center;

      h1 {
        margin: 0;
        padding-right: 1rem;
        color: #fffea7;
      }
    }
    
    .subtitle {
      padding: 0 0 0 1rem;
      font-size: 18px;
      color: rgba(255,255,255,0.6);
      flex: 1;
      position: relative;
    }
  }

  &::after {
    width: 100%;
    height: 50px;
    position: absolute;
    content: "";
    bottom: -25px;
    z-index: 0;
    left: 0;
    background: #fff;
    -ms-transform: rotate(-0.5deg);
    -webkit-transform: rotate(-0.5deg);
    transform: rotate(-0.5deg);
  }
  
  .btn-header {
    text-transform: uppercase;
    background: #222;
    box-shadow: inset 0px 0px 4px rgba(0,0,0,0.4);
    color: #fff;
    font-family: 'Roboto', sans-serif;
    display: flex;
    align-items: center;
    padding: 10px 20px;
    line-height: 20px;
    font-size: 14px;
    border-radius: 20px;
    border: 0;
    cursor: pointer;
    outline: none;
    
    input {
      margin: 0 10px 0 0;
    }

    &:hover {
      background-color: #000;
    }
  }
`;

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange() {
    this.props.dispatch({type: SET_ADMIN_MODE, payload: !this.props.adminModeIsOn });
  }

  render () {
    return (
      <HeaderWrapper>
        <div className={'headerWrapper'}>
          <div className={'title'}>
            <h1>{this.props.title}</h1>
          </div>
          <span className={'subtitle'}>Interactive page builder</span>
          <label className="btn btn-header">
            <input type="checkbox" checked={this.props.adminModeIsOn} onChange={this.handleChange} />
            Edit mode
          </label>
        </div>
        <Nav />
      </HeaderWrapper>
    )
  }
}

Header.defaultProps = {
  title: 'Project name',
  loader: false,
  adminModeIsOn: false
};

Header.proptypes = {
  title: PropTypes.string,
  adminModeIsOn: PropTypes.bool
};

const mapStateToProps = (state) => {
  return {
    adminModeIsOn: state.adminMode.isOn,
  };
};

export default connect(mapStateToProps)(Header)
