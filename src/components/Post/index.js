import React from 'react';
import { Link } from '../../routes';
import styled from "styled-components";

const PostWrapper = styled.div`
  a {
    text-decoration: none;
    display: block;
    color: #333;
    border-bottom: 1px solid #eee;
    padding-bottom: 1rem;
    margin-bottom: 1rem;
    
    h3 {
      font-size: 26px;
    }
    
    &:last-child {
      border-bottom: 0;
    }

    &:hover {
      h3 { color: #CD41E0 }
    }
  }

  h3 {
    font-size: 1.75rem;
    text-transform: capitalize;
    margin: 0;
  }

  p {
    color: #777;
    margin: 0;
  }
`;

const PostItem = ({ post }) => (
  <PostWrapper>
    <Link route='post' params={{ slug: post.slug }}>
      <a>
        <h3>{post.title}</h3>
        <p>{post.body}</p>
      </a>
    </Link>
  </PostWrapper>
);

export default PostItem;
