import styled from 'styled-components';

const Footer = styled.footer`
  padding: 2rem 2rem 1rem;
  background: #ffffff;
  background: -moz-linear-gradient(top, #ffffff 0%, #eeeeee 100%);
  background: -webkit-linear-gradient(top, #ffffff 0%,#eeeeee 100%);
  background: linear-gradient(to bottom, #ffffff 0%,#eeeeee 100%);
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#eeeeee',GradientType=0 );
  color: #514756;
  text-align: center;
`;

export default Footer;
