import styled from 'styled-components'
import React from 'react';

const H1 = styled.h1`
  color: #514756;
  margin: 0;
  padding: 1rem 2rem 0;
`;

class Title extends React.Component {
  render () {
    return (
      <H1>{this.props.children}</H1>
    )
  }
}

export default Title
