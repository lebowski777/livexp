import styled from 'styled-components';

const Loader = styled.div`
  border: 8px solid #f3f3f3;
  border-top: 8px solid #863AF2;
  border-radius: 50%;
  position: fixed;
  left: 50%;
  top: 50%;
  margin: -25px 0 0 -25px;
  background: #fff;
  width: 50px;
  height: 50px;
  animation: spin 2s linear infinite;

  @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
  }
`;

export default Loader;
