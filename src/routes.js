const nextRoutes = require('next-routes');

const routes = module.exports = nextRoutes();

routes.add('index', '/');
routes.add('page', '/:page');
routes.add('post', '/post/:slug');
