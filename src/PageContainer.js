import { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { fetchPages } from './store/actions/pages';
import { connect } from 'react-redux';

const nop = () => {};

class PageContainer extends PureComponent {
  static async getInitialProps({ store }) {
    console.log('getInitial');
    const pages = await store.dispatch(fetchPages());
    return { pages };
  }

  render() {
    const { pages, children } = this.props;
    return children(pages || []);
  }
}

PageContainer.defaultProps = {
  pages: [],
  children: nop,
};

PageContainer.proptypes = {
  pages: PropTypes.arrayOf(PropTypes.shape({})),
  children: PropTypes.func
};

const mapStateToProps = (state) => {
  return {
    pages: state.posts.list
  };
};

export default connect(mapStateToProps)(PageContainer)
