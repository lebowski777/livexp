import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createRouterMiddleware, initialRouterState } from 'connected-next-router';
import thunkMiddleware from 'redux-thunk';
import reducers from './reducers/index';

const routerMiddleware = createRouterMiddleware();

export const makeStore = (initialState = {}, options) => {
  if (options.isServer) {
    initialState.router = initialRouterState(options.asPath);
  }

  return createStore(
    reducers,
    initialState,
    composeWithDevTools(
      applyMiddleware(
        routerMiddleware,
        thunkMiddleware
      )
    )
  );
};
