import { SET_ADMIN_MODE } from '../actions/index';

const initState = {
  isOn: false
};

export const adminMode = (state = initState, action) => {
  switch(action.type) {
    case SET_ADMIN_MODE:
      return Object.assign({}, state, {
        isOn: action.payload
      });
    default: return state;
  }
};
