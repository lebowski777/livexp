import { SHOW_LOADER, HIDE_LOADER } from '../actions/index';

const initState = {
  loader: false
};

export const loader = (state = initState, action) => {
  switch(action.type) {
    case SHOW_LOADER:
      return Object.assign({}, state, {
        loader: true
      });
    case HIDE_LOADER:
      return Object.assign({}, state, {
        loader: false,
      });
    default: return state;
  }
};
