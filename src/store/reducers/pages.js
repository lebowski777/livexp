import { GET_PAGES_SUCCESS } from '../actions/index';

const initState = {
  list: []
};

export const pages = (state = initState, action) => {
  switch(action.type) {
    case GET_PAGES_SUCCESS:
      return Object.assign({}, state, {
        list: action.payload
      });
    default: return state;
  }
};
