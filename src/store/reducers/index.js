import { combineReducers } from 'redux';
import { posts } from './posts';
import { pages } from './pages';
import { loader } from './loader';
import { adminMode } from './adminMode';
import { routerReducer } from 'connected-next-router';

export default combineReducers({
  pages,
  posts,
  loader,
  adminMode,
  router: routerReducer
});
