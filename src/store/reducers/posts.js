import { GET_POSTS_SUCCESS } from '../actions/index';

const initState = {
  list: []
};

export const posts = (state = initState, action) => {
  switch(action.type) {
    case GET_POSTS_SUCCESS:
      return Object.assign({}, state, {
        list: action.payload
      });
    default: return state;
  }
};
