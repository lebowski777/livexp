import { loadDB } from '../../firebase/index';
import { GET_POSTS_SUCCESS, HIDE_LOADER, SHOW_LOADER } from './index';
const firebase = loadDB();

export function fetchPosts(postSlug = null) {
  const posts = [];
  let collection = null;

  return async dispatch => {
    dispatch({type: SHOW_LOADER });

    if(postSlug){
      collection = firebase.firestore().collection('posts').where('slug', '==', postSlug);
    } else {
      collection = firebase.firestore().collection('posts');
    }

    await collection.get().then(function (doc) {
      doc.forEach(function (post) {
        posts.push(post.data());
      });
      dispatch({type: GET_POSTS_SUCCESS, payload: posts });
      dispatch({type: HIDE_LOADER });
    });
  }
}
