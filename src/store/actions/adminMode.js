import { SET_ADMIN_MODE } from './index';

export function setAdminMode(adminMode) {
  return dispatch => {
    dispatch({type: SET_ADMIN_MODE, payload: adminMode });
  }
}
