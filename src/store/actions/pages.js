import { loadDB } from '../../firebase/index';
import { GET_PAGES_SUCCESS, HIDE_LOADER, SHOW_LOADER } from './index';
import { comparePagesByPosition } from '../../helpers';
const firebase = loadDB();

export function fetchPages(pageSlug = null) {
  const pages = [];
  let collection = null;

  return async dispatch => {
    dispatch({type: SHOW_LOADER });

    if(pageSlug){
      collection = firebase.firestore().collection('pages').where('slug', '==', pageSlug);
    } else {
      collection = firebase.firestore().collection('pages');
    }

    await collection.get().then(function (doc) {
      doc.forEach(function (page) {
        pages.push(page.data());
      });
      const sortedPages = pages.sort(comparePagesByPosition);

      dispatch({type: GET_PAGES_SUCCESS, payload: sortedPages });
      dispatch({type: HIDE_LOADER });
    });
  }
}
