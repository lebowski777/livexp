FROM node:latest

# Db
ENV NODE_ENV=development
ENV DATABASE=mongodb://mongodb:27017/livexp
ENV PORT=3001

RUN npm install -g nodemon

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json /usr/src/app/
RUN npm install

# Bundle app source
COPY . /usr/src/app
RUN npm run build

EXPOSE 3000

CMD [ "npm", "run", "dev" ]
